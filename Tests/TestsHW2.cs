﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ClassesForTesting;

namespace Tests
{
    [TestFixture, Description("Tests for the HWAuto2")]
    class TestsHW2
    {
        //ScientificCalculator Calk_HW_2 = new ScientificCalculator();
        ScientificCalculator Calk_HW_2 ;
        [OneTimeSetUp]
        public void Creations()
        {
             Calk_HW_2 = new ScientificCalculator();
        }

        [OneTimeTearDown]
        public void Negative_ScientificCalk_MassiveMax_TearDown()
        {
            Calk_HW_2 =null;
        }


        
        [TestCase(1, 4, ExpectedResult = 5)]
        public double Positive_Add_ExpectedResult(int n, int d)
        {
            return (Calk_HW_2.Add(1,4));
        }

        [TestCase(3, 3, ExpectedResult = 7)]
        public double Negative_Add_ExpectedResult(int n, int d)
        {
            return (Calk_HW_2.Add(3, 7));
        }

        [TestCase(2, 2, 4)]
        [TestCase(6, 6, 12)]
        [TestCase(1, -3, -2)]
        [TestCase(2.5, 100, 102.5)]
        [Order(1)]
        public void HW_2_Positive_Calk_Add_(double x, double y, double res)
        {
            Assert.AreEqual(res, Calk_HW_2.Add(x, y));
        }


        [TestCase(14, 16, 30)]
        [Ignore("Ignore the test.")]

        public void HW_2_Positive_Add(double x, double y, double res)
        {
            Assert.AreEqual(res, Calk_HW_2.Add(x, y));
        }


        [TestCase(5, 5, 26), Repeat(3)]
        public void HW_2_Negative_Multiply(double x, double y, double res)
        {
            Assert.AreEqual(res, Calk_HW_2.Multiply(x, y));
        }



        //  [Retry(2)]
        [Order(2)]
        public void HW_2_Negative_Subtract()
        {
            Assert.AreEqual(Calk_HW_2.Subtract(8, 8), 7);
        }

        [Test]
        [Order(3)]
        public void HW_2_Positive_ScientificCalk_MassiveMax()
        {
            int[] x = { 1, 2, 3 };
            Assert.Pass("Pass");
        }

        [Test]
        [Order(4)]
        public void HW_2_Negative_ScientificCalk_MassiveMax()
        {
            int[] x = { 1, 2, 3 };
            Assert.Fail("Fail");
        }

        [Test]
        [Order(5)]
        public void HW_2_Positive_ScientificCalk_MassiveMax_IsNotEmpty()
        {
            int[] x = { 1, 2, 3 };
            Assert.IsNotEmpty(x);
        }

        [Test]
        public void HW_2_Positive_IsNaN()
        {
            Assert.IsNaN(Math.Sqrt(-2));
            
        }


        //String asserts
        [Test]
        public void HW_2_Positive__String_Assert_1()
        {
            StringAssert.Contains("yes","yes"); 
        }

        [Test]
        public void HW_2_Positive__String_Assert_2()
        {
            StringAssert.AreEqualIgnoringCase("yes", "Yes");
        }

        [Test]
        public void HW_2_Negative__String_Assert_3()
        {
            StringAssert.Contains("yes", "Yes");
        }

        [Test]
        public void HW_2_Negative__String_Assert_4()
        {
            StringAssert.Contains("yes", "no");
        }

        [Test]
        public void HW_2_Positive__String_Assert_5()
        {
            StringAssert.StartsWith("123", "123456");
        }

        [Test]
        public void HW_2_Positive__String_Assert_6()
        {
            StringAssert.EndsWith("456","123456");
        }


        //Constraints

        [Test]
        public void HW_2_Positive_ScientificCalk_Pow_Constraint()
        {
            //Assert.AreEqual(Calk_HW_2.Pow(2, 4), 16, " 2 ^ 4 should be equal to 16");
            Assert.That(Calk_HW_2.Pow(2, 4), Is.EqualTo(16));
        }

        [Test]
        
        public void HW_2_Negative_ScientificCalk_MassiveMax_Constraint()
        {
            int[] x = { 1, 2, 3 };
            Assert.That(Calk_HW_2.MassiveMax(x), Is.Empty);
        }

        [Test]
        public void HW_2_Positive_ScientificCalk_IsPositiv_Constraint()
        {
            Assert.That(Calk_HW_2.IsPositive(5), Is.True);
        }



    }
}
