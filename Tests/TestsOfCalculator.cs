﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ClassesForTesting;

namespace Tests
{
    public class TestsOfCalculator
    { //There are tests of the Calculator here.
        Calculator Calk1 = new Calculator();

        //HW_1

        // Positive tests
        [Test]
        
        public void Positive_Calk_Add()
        {
            Assert.AreEqual(Calk1.Add(2, 2), 4, "2 + 2 should be equal to 4");
        }

        [Test]
        public void Positive_Calk_Subtract()
        {
            Assert.AreEqual(Calk1.Subtract(5, 5), 0, "5 - 5 should be equal to 0");
        }

        [Test]
        public void Positive_Calk_Multiply()
        {
            Assert.AreEqual(Calk1.Multiply(6, 6), 36, "6*6 should be equal to 36");
        }

        [Test]
        public void Positive_Calk_Divide()
        {
            Assert.AreEqual(Calk1.Divide(10, 0.1), 100, "10/0.1 should be equal to 100");
        }

        [Test]
        public void Positive_Calk_IsPossitive()
        {
            Assert.AreEqual(Calk1.IsPositive(10), true, "true should be equal to true");
        }

        //Negative

        [Test]
        public void Negative_Calk_Add()
        {
            Assert.AreEqual(Calk1.Add(2, 2), 3, "2 + 2 should be equal to 3");
        }

        [Test]
        public void Negative_Calk_Subtract()
        {
            Assert.AreEqual(Calk1.Subtract(8, 8), 7, "8 - 8 should be equal to 7");
        }

        [Test]
        public void Negative_Calk_Multiply()
        {
            Assert.AreEqual(Calk1.Multiply(10, 10), 99, "10 * 10 should be equal to 99");
        }

        [Test]
        public void Negative_Calk_Divide()
        {
            Assert.AreEqual(Calk1.Divide(50, 5), 0.45, "50 / 5 should be equal to 0.45");
        }

        [Test]
        public void Negative_Calk_IsPossitive()
        {
            Assert.AreEqual(Calk1.IsPositive(-10), true, "true should be equal to true");
        }


    }
}
