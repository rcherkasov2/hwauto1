﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ClassesForTesting;

namespace Tests
{
    class TestsOfScientificCalculator
    {
        ScientificCalculator Calk2 = new ScientificCalculator();

        //There are tests of the Scientific Calculator here.

        // Positive testing.
        [Test]
        public void Positive_ScientificCalk_Pow()
        {
            Assert.AreEqual(Calk2.Pow(2, 4),16, " 2 ^ 4 should be equal to 16");
        }

        [Test]
        public void Positive_ScientificCalk_Percent()
        {
            Assert.AreEqual(Calk2.Percent(5,20), 0.25, " 5 is 25 %  of 20");
        }

        [Test]
        public void Positive_ScientificCalk_Abs()
        {
            Assert.IsTrue(Calk2.IsPositive(Calk2.Abs(3)), " Abs of 3 is possitive.");
        }

        [Test]
        public void Positive_ScientificCalk_MassiveMax()
        {
            int[] x = { 1, 2, 3 };
            Assert.AreEqual(Calk2.MassiveMax(x), 3, " the maximum value of the array should be equal to 3");
        }

        [Test]
        public void Positive_ScientificCalk_MassiveMin()
        {
            int[] x = { 1, 2, 3 };
            Assert.AreEqual(Calk2.MassiveMin(x), 1, " the minimum value of the array should be equal to 1");
        }


        //Negative testing .

        [Test]
        public void Negative_ScientificCalk_Pow()
        {
            Assert.AreNotEqual(Calk2.Pow(5, 2), 25, " 5 ^ 2 should not be equal to 25");
        }

        [Test]
        public void Negative_ScientificCalk_Percent()
        {
            Assert.AreNotEqual(Calk2.Percent(10, 20), 0.5, " 10 is not 50 %  of 20");
        }

        [Test]
        public void Negative_ScientificCalk_Abs()
        {
            Assert.AreEqual((Calk2.IsPositive(Calk2.Abs(-3))),false, " Abs of -3 is negative");
            
        }

        [Test]
       public void Negative_ScientificCalk_MassiveMax()
        {
            int[] x = { 1, 2, 3 };
            Assert.AreEqual(Calk2.MassiveMax(x), 1, " the maximum value of the array should be equal to 1");
        }

        [Test]
        public void Negative_ScientificCalk_MassiveMin()
        {
            int[] x = { 1, 2, 3 };
            Assert.AreNotEqual(Calk2.MassiveMin(x), 1, " the minimum value of the array should not be equal to 1");
        }
    }
}
















