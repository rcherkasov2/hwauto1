﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesForTesting
{
    public class ScientificCalculator : Calculator
    {
        public double Pow(double x, int y)
        {
            double pow = x;

            if (y == 0)
            {
                pow = 1;
            }
            else if (y < 0)
            {
                double temp = 0;
                y = Abs(y);
                for (int i = 1; i < y; i++)
                {
                    pow = Multiply(pow, x);
                    temp = pow;
                }
                pow = 1 / temp;
            }
            else
                for (int i = 1; i < y; i++)
                {
                    pow = Multiply(pow, x);
                }
            return pow;
        }

        public double Percent(double x, double y)
        {
            double percent = x / y * (100 / 100);
            return percent;
        }

        public int Abs(int x)
        {
            int abs = x;

            if (x < 0)
            {
                abs = x * (-1);
            }

            return abs;
        }

        public int MassiveMax(int[] array)
        {
            int max;
            max = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > max)
                    max = array[i];
            }
            return max;
        }

        public int MassiveMin(int[] array)
        {
            int min;
            min = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < min)
                    min= array[i];
            }
            return min;
        }
    }
}
