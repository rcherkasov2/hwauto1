﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesForTesting
{
    public class Calculator
    {
        public double Add(double x, double y)
        {
            double z = x + y;
            return z;
        }
        public double Subtract(double x, double y)
        {
            double z = x - y;
            return z;
        }
        public double Multiply(double x, double y)
        {
            double z = x * y;
            return z;
        }
        public double Divide(double x, double y)
        {
            double z = x / y;
            return z;
        }
        public bool IsPositive(int x)
        {
            bool pos=false ;
            if (x >= 0)
            {
                pos = true;
            }
            return pos;
        }
    }
}